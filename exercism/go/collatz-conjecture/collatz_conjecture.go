// Package collatzconjecture implements a routine for finding the number of steps
// required to satisfy the collatz conjecture for any int n.
package collatzconjecture

import (
	"fmt"
	"errors"
)

// CollatzConjecture tests that n satisfies the collatz conjecture, returning the number of steps required.
func CollatzConjecture(n int) (int, error) {
	if n <= 0 {
		return 0, errors.New(fmt.Sprint("expected a positive number but got ", n))
	}

	steps := 0
	for {
		if n == 1 {
			return steps, nil
		}

		if n%2 == 0 {
			n = n/2
		} else {
			n = n*3 + 1
		}
		steps++
	}
}
