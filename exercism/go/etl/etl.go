// Package etl provides a routine for transforming a legacy scrabble scoring table into the modern format.
package etl

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

// Transform converts a map scoreLettersTable of score to letter set into a map
// from letters to their respective scores.  Each string of the input table is
// expected to be a single letter in the range 'A' to 'Z'.
func Transform(scoreLettersTable map[int][]string) map[string]int {
	letterScores := map[string]int{}
	for s, ws := range scoreLettersTable {
		for _, w := range ws {
			r, _ := utf8.DecodeRuneInString(w)
			if r == utf8.RuneError {
				panic(fmt.Sprint("couldn't decode first letter when transforming `", w, "'"))
			}

			c := unicode.ToLower(r)
			if c < 'a' || c > 'z' {
				panic(fmt.Sprint("unknown scrabble letter `", c, "'"))
			}

			if s2, exists := letterScores[string(c)]; exists && s2 != s {
				panic(fmt.Sprint("letter `", c, "' has distinct scores `", s, "' and `", s2, "'"))
			}

			letterScores[string(c)] = s
		}
	}

	return letterScores
}
