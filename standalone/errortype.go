package main

import (
  "fmt"
  "encoding/json"
  "errors"
)

func main() {
  err := doSomethingThatMightFail(43)
  if err != nil {
    fmt.Println("Ooops:", err)
  }
  fmt.Println("internet")

  printTheJsonIfPossible()
}

func doSomethingThatMightFail(someNum int) error {
  if someNum != 42 {
    return errors.New("invalid number")
  }
  return nil
}

func printTheJsonIfPossible() {
  jsonObj := map[string]string{}
  err := json.Unmarshal([]byte("This isn't valid json!"), &jsonObj)
  if err != nil {
    fmt.Println("Dang.")
    return
  }
  fmt.Println("successful parse")
}
