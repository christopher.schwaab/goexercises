package main

import (
	"fmt"
)

func main() {
	ints := make(chan int)

	go inc(21, ints)
	go inc(42, ints)
	go inc(12, ints)

	fmt.Println(<-ints, <-ints, <-ints)

	strs := make(chan string)
	strs2 := make(chan string)

	go func() { ints <- 0 }()
	go func() { strs <- "woooo" }()

	select {
	case i := <- ints:
		fmt.Println("i =", i)
	case <-strs:
		fmt.Println("got a str")
	case <-strs2:
		fmt.Println("never happens")
	}
}

func inc(i int, c chan int) {
	c <- i + 1
}
