package main

import (
	"fmt"
)

func main() {
	colorsMap := map[string]string {
		"AliceBlue":    "#f0f8ff",
		"AntiqueWhite": "#faebd7",
		"Aqua":         "#00ffff",
		"Aquamarine":   "#7fffd4",
		"Azure":        "#f0ffff",
		"Red":          "#ff0000",
		"Green":        "#00ff00",
		"White":        "#ffffff",
	}

	fmt.Println("colorsMap:", colorsMap)

	fmt.Println("AliceBlue:", colorsMap["AliceBlue"])

	colorsMap["Beige"] = "#f5f5dc"

	fmt.Println("This key 'Bisque' doesn't exist:", colorsMap["Bisque"])

	if val, exists := colorsMap["Foobar"]; exists {
		fmt.Println("Foobar is a color? with the hex value:", val)
	}

	delete(colorsMap, "Azure")

	fmt.Println("colorsMap:", colorsMap)
}
