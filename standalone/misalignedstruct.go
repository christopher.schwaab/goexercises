package main

// #cgo LDFLAGS: -L${SRCDIR}/lib -lmisalignedstruct
// #include "misalignedstruct.h"
import "C"

import (
	"fmt"
	"reflect"
	"unsafe"
)

type alignedStruct struct {
	off0 uint16
	off2 uint8
	off6 uint32
	off7 uint8
	off8 uint32
}

type sliceParts struct {
	ptr unsafe.Pointer
	len uintptr
	cap uintptr
}

func UnsafeUnmarshal(
	tDst reflect.Type,
	tSrc reflect.Type,
	dst unsafe.Pointer,
	src unsafe.Pointer,
) {
	srcBytes := C.GoBytes(src, C.int(tSrc.Size()))
	var dstBytes []byte
	oldParts := writeSlice(unsafe.Pointer(&dstBytes), dst, tDst.Size(), tDst.Size())
	defer writeSlice(unsafe.Pointer(&dstBytes), oldParts.ptr, oldParts.len, oldParts.cap)

	fmt.Println(fmt.Sprintf("unmarshal %p to dstBytes@%p", dst, &dstBytes[0]))

	for i := 0; i < tDst.NumField(); i++ {
		fmt.Println(fmt.Sprintf("unmarshal field %d", i))
		dstField := tDst.Field(i)
		srcField := tSrc.Field(i)
		copy(
			dstBytes[dstField.Offset:dstField.Offset+dstField.Type.Size()],
			srcBytes[srcField.Offset:srcField.Offset+srcField.Type.Size()],
		)
	}
}

func writeSlice(s unsafe.Pointer, p unsafe.Pointer, sz uintptr, cap uintptr) sliceParts {
	raw := sliceParts{
		ptr: p,
		len: sz,
		cap: cap,
	}

	oldParts := sliceParts{
		ptr: (*sliceParts)(s).ptr,
		len: (*sliceParts)(s).len,
		cap: (*sliceParts)(s).cap,
	}
	copy(
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(s))[:],
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(unsafe.Pointer(&raw)))[:],
	)

	return oldParts
}

func (src *C.struct_misaligned_struct) Unmarshal(dst *alignedStruct) {
	UnsafeUnmarshal(
		reflect.TypeOf(*dst),
		reflect.TypeOf(*src),
		unsafe.Pointer(dst),
		unsafe.Pointer(src),
	)
}

func dumpSlice(x, y, z byte) {
	xs := make([]byte, 0, 8)
	xs = append(xs, x)
	xs = append(xs, y)
	xs = append(xs, z)

	var raw sliceParts

	copy(
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(unsafe.Pointer(&raw)))[:],
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(unsafe.Pointer(&xs)))[:],
	)

	fmt.Println(
		"raw.ptr = ", raw.ptr,
		", raw.len = ", raw.len,
		", raw.cap = ", raw.cap,
	)
}

func main() {
	dumpSlice(0xbe, 0xee, 0xef)

	x := C.mkmisaligned()
	t := reflect.TypeOf(x)
	fmt.Println("type: ", t)

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		fmt.Println(f.Name, " : ", f.Type.Name(), " kind ", f.Type.Kind())
		if f.Tag != "" {
			fmt.Println("  tag1=", f.Tag.Get("tag1"), ", tag2=", f.Tag.Get("tag2"))
		}
	}

	var aligned alignedStruct
	x.Unmarshal(&aligned)
	fmt.Println(
		fmt.Sprintf(
			"%p %x\n%x\n%x\n%x\n%x",
			&aligned,
			aligned.off0,
			aligned.off2,
			aligned.off6,
			aligned.off7,
			aligned.off8))
}
