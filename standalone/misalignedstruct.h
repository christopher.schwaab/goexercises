#ifndef __MISALIGNEDSTRUCT_H_
#define __MISALIGNEDSTRUCT_H_

#ifdef _MSC_VER
#  define PACK_STRUCT
#else
#  define PACK_STRUCT __attribute__((packed))
#endif

#include <stdint.h>

#ifdef _MSC_VER
#  pragma pack(push, 1)
#endif

struct misaligned_struct {
    uint16_t off0;
    uint8_t off2;
    uint32_t off3;
    uint8_t off7;
    uint32_t off8;
} PACK_STRUCT;

#ifdef _MSC_VER
#  pragma pack(pop)
#endif

struct misaligned_struct mkmisaligned(void);

#endif // __MISALIGNEDSTRUCT_H_
