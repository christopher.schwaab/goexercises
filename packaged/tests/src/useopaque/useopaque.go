package main

import (
	"opaquestr"
)

// build error on use
//func printPStr(s opaquestr.pStr) {
//	opaquestr.PrintPStr(s)
//}

func main() {
	s := opaquestr.MkStr("hi s")
	sstr := opaquestr.Str("hi sstr")
	opaquestr.PrintStr(s)
	opaquestr.PrintStr(sstr)
	opaquestr.PrintStr("hi str lit")

	s2 := opaquestr.MkStr2("hi s2")
	sstr2 := opaquestr.Str2("hi sstr2")
	opaquestr.PrintStr2(s2)
	opaquestr.PrintStr2(sstr2)
	opaquestr.PrintStr2("hi str2 lit")

	// build error on use
	//printPStr(opaquestr.pStr("hello pstr"))
}
