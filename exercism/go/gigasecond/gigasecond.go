/*
Package gigasecond provides a utility function for adding a gigasecond to time.
*/
package gigasecond

import "time"

// Add a gigasecond (i.e. 1e9) to time.
func AddGigasecond(t time.Time) time.Time {
	return t.Add(time.Second * time.Duration(1e9))
}
