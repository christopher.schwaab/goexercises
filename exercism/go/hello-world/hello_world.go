/*
Package greeting implements a utility routine for constructing greeting strings.
*/
package greeting

// Constructs a hello world greeting message.
func HelloWorld() string {
	return "Hello, World!"
}
