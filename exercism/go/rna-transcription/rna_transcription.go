package strand

import (
	"fmt"
	"errors"
)

func dnaNucleotideToRnaNucleotide(n byte) byte {
	switch n {
	case 'G':
		return 'C'
	case 'C':
		return 'G'
	case 'T':
		return 'A'
	case 'A':
		return 'U'
	default:
		panic(errors.New(fmt.Sprint("unrecognized nucleotide ", n)))
	}
}

// ToRNA transcribes the RNA complement of the given dna strand.
func ToRNA(dna string) string {
	rna := make([]byte, len(dna))
	for i := range dna {
		rna[i] = dnaNucleotideToRnaNucleotide(dna[i])
	}

	return string(rna)
}
