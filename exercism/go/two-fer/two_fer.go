/*
Package twofer implements a utility routine for constructing strings about sharing.
*/
package twofer

import "fmt"

// ShareWith constructs the canonical sharing message between "me" and name.
func ShareWith(name string) string {
	if name == "" {
		name = "you"
	}
	return fmt.Sprint("One for ", name, ", one for me.")
}
