// Package proverb provides a routine for constructing proverbs from lists of words.
package proverb

import (
	"fmt"
)

// Proverb constructs an extended "For Want of a Nail" proverb from the words in rhyme.
func Proverb(rhyme []string) []string {
	if len(rhyme) == 0 {
		return []string{}
	}

	word0 := rhyme[0]
	lastWord := word0
	proverb := make([]string, len(rhyme))

	for i, word := range rhyme[1:] {
		proverb[i] = fmt.Sprint("For want of a ", lastWord, " the ", word, " was lost.")
		lastWord = word
	}
	proverb[len(rhyme)-1] = fmt.Sprint("And all for the want of a ", word0, ".")

	return proverb
}
