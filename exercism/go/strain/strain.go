package strain

type Ints []int
type Lists [][]int
type Strings []string

func (xs Ints) Keep(p func(int) bool) Ints {
	if xs == nil {
		return nil
	}

	ys := make(Ints, 0, len(xs))
	for _, x := range xs {
		if p(x) {
			ys = append(ys, x)
		}
	}

	return ys
}

func (xs Ints) Discard(p func(int) bool) Ints {
	if xs == nil {
		return nil
	}

	return xs.Keep(func (x int) bool { return !p(x) })
}

func (xss Lists) Keep(p func([]int) bool) Lists {
	if xss == nil {
		return nil
	}

	yss := make(Lists, 0, len(xss))
	for _, xs := range xss {
		if p(xs) {
			yss = append(yss, xs)
		}
	}

	return yss
}

func (ss Strings) Keep(p func(string) bool) Strings {
	if ss == nil {
		return nil
	}

	ys := make(Strings, 0, len(ss))
	for _, s := range ss {
		if p(s) {
			ys = append(ys, s)
		}
	}

	return ys
}
