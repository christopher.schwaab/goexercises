package main

import (
	"fmt"
)

type Exp interface {
	Lam(x int) (func() )
}
