package main

import (
	"fmt"
)

func main() {
	myBool := true
	fmt.Println("myBool: ", myBool)
	var err error = nil
	fmt.Println("error?: ", err)

	var helloWorldStr = "Hello, \n\"\"sup"
	fmt.Println(helloWorldStr)
	helloWorldStr = `dang
string literal`

	myInt := 42
	fmt.Println("myInt: ", myInt)

	var myInt64 int64 = 21
	fmt.Println("myInt64: ", myInt64)

	myFloat64 := float64(42.42)
	fmt.Println("inferred myFloat64: ", myFloat64)

	var myFloat264 float64 = 42.42
	fmt.Println("myFloat64: ", myFloat264)

	myFloat := 42.42
	fmt.Println("myFloat: ", myFloat)
}
