package main

import (
	"fmt"
)

func main() {
	demoSlice := make([]string, 0, 10)
	fmt.Println("Empty slice:", demoSlice, "with len: ", len(demoSlice))

	demoSlice = make([]string, 10)
	fmt.Println("Slice filled with defaults: ", demoSlice, "with len:", len(demoSlice))

	demoSlice[0] = "21"
	demoSlice[9] = "42"

	fmt.Println("Get a slice of a slice:", demoSlice[:3])
	fmt.Println("Another slice of a slice:", demoSlice[3:])
	fmt.Println("yet another slice of a slice:", demoSlice[2:6])
	fmt.Println("Useless slice of a slice:", demoSlice[:])

	letsAppendSlice := []string{"foo", "appendable"}
	demoSlice = append(demoSlice, letsAppendSlice...)
	fmt.Println("Updated slice:", demoSlice)
}
