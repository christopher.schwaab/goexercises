package space

import (
	"errors"
	"fmt"
)

type Planet string
type validPlanet Planet

const (
	Mercury = "Mercury"
	Venus   = "Venus"
	Earth   = "Earth"
	Mars    = "Mars"
	Jupiter = "Jupiter"
	Saturn  = "Saturn"
	Uranus  = "Uranus"
	Neptune = "Neptune"
)

func isValidPlanet(planet Planet) (validPlanet, error) {
	switch planet {
	case Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune:
		return validPlanet(planet), nil
	default:
		return "", errors.New(fmt.Sprint(planet, "is not a planet"))
	}
}

func secondsToEarthYears(seconds float64) (earthYears float64) {
	const daysPerYear = 365.25
	const hoursPerDay = 24
	const minutesPerHour = 60
	const secondsPerMinute = 60
	const secondsPerYear = daysPerYear*hoursPerDay*minutesPerHour*secondsPerMinute
	const yearsPerSecond = 1.0 / secondsPerYear
	return seconds*yearsPerSecond
}

func orbitalPeriod(planet validPlanet) (earthYears float64) {
	switch planet {
	case Mercury:
		return 0.2408467
	case Venus:
		return 0.61519726
	case Earth:
		return 1.0
	case Mars:
		return 1.8808158
	case Jupiter:
		return 11.862615
	case Saturn:
		return 29.447498
	case Uranus:
		return 84.016846
	case Neptune:
		return 164.79132
	}

	panic(fmt.Sprint("the impossible happened, the validPlanet", planet, "is invalid"))
	return
}

func Age(seconds float64, planet Planet) float64 {
	okPlanet, _ := isValidPlanet(planet)
	return secondsToEarthYears(seconds) / orbitalPeriod(okPlanet)
}
