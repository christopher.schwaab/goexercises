package main

import (
	"fmt"
	"net/http"
	"os"
)

type miniHttpServer struct {}

func (s miniHttpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Mini http server in go!"))
}

func main() {
	fmt.Println("Listening on port 8080 ...")
	if err := http.ListenAndServe(":8080", miniHttpServer{}); err != nil {
		fmt.Println("An error occurred listening on port 8080:", err)
		os.Exit(1)
	}
	fmt.Println("Exited gracefully")
}
