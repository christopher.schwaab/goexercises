package main

import (
	"fmt"
)

type Vehicle struct {
	Make  string
	Model string
}

type Car struct {
	VerticalDoors bool
	Vehicle
}

func main() {
	teslaModelX := Car {
		VerticalDoors: true,
			Vehicle: Vehicle {
			Make: "Tesla",
				Model: "X",
	  },
	}

	fmt.Println(teslaModelX)
	fmt.Println(teslaModelX.Model)
}
