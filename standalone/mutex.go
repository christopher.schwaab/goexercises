package main

import (
	"fmt"
	"sync"
)

var sumOfSquares = 0
var sumOfSquaresMutex sync.Mutex

func main() {
	var wg sync.WaitGroup

	for i := 1; i < 11; i++ {
		wg.Add(1)
		go addNumber(i, i*10+i, &wg)
	}

	wg.Wait()
	fmt.Println("All goroutines done, sum is:", sumOfSquares)
}

func addNumber(idx, n int, wg *sync.WaitGroup) {
	defer wg.Done()
	sq := n*n

	fmt.Println("In goroutine number", idx, "and we found that n*n=", sq)
	incrementSumOfSquares(sq)
}

func incrementSumOfSquares(n int) {
	defer sumOfSquaresMutex.Unlock()
	sumOfSquaresMutex.Lock()
	sumOfSquares += n
}
