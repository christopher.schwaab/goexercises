package main

import (
	"fmt"
)

func main() {
	var myString = "Hello, ɛλλο"
	fmt.Println([]byte(myString))
	fmt.Println(string([]byte(myString)))

	var i64 int64 = 999999999999
	var f64 float64 = 99.95
	var smallInt int32 = 42

	fmt.Println("Float to in (works but could lose data): ", int32(f64))
	fmt.Println("Int64 to int32: ", int32(i64))

	fmt.Println("Small to large casting is fine: ", int64(smallInt))
	fmt.Println("Oink: ", float64(smallInt))
}
