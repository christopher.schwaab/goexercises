/*
Package hamming provides the hamming distance function for strings.
*/
package hamming

import (
	"errors"
	"fmt"
)

// Distance computes the hamming distance between the strings a and b or
// produces an error when it doesn't exist.
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New(fmt.Sprint("The strings `", a, "' and `", b, "' are of unequal length."))
	}

	d := 0
	for i := range a {
		if a[i] != b[i] {
			d++
		}
	}

	return d, nil
}
