package main

import (
	"fmt"
)

func main() {
	fmt.Println(exampleVariadic("This", "Will", "Accept", "Many", "Args"))
	fmt.Println(exampleVariadic())
}

func exampleVariadic(args... string) []string {
	return args
}
