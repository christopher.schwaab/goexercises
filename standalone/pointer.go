package main

import (
	"fmt"
)

type FooStruct struct {
	Children []*FooChild
}

type FooChild struct {
	SampleVal int
}

func main() {
	ptrToFoo := giveMePointer()
	fmt.Println(ptrToFoo)
	fmt.Println("Children:", ptrToFoo.Children)
	for _, child := range ptrToFoo.Children {
		fmt.Println("Child (ptr):", child, "Dereferenced:", *child)
	}

	var thisIsNil *FooStruct
	fmt.Println(thisIsNil.Children)
}

func giveMePointer() *FooStruct {
	foo := FooStruct {
		Children: make([]*FooChild, 10),
	}

	for i := range foo.Children {
		foo.Children[i] = &FooChild {
			SampleVal: 42+i,
		}
	}

	return &foo
}
