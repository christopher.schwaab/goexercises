package main

import "C"
import (
	"reflect"
	"unsafe"
)

type sliceParts struct {
	ptr unsafe.Pointer
	len uintptr
	cap uintptr
}

func UnsafeUnmarshal(
	tDst reflect.Type,
	tSrc reflect.Type,
	dst unsafe.Pointer,
	src unsafe.Pointer,
) error {
	if tDst.NumField() != tSrc.NumField() {
		return fmt.Errorf(
			"destination type '%s' and source type '%s' have different numbers of fields: '%d' and '%d' respectively",
			tDst.Name(),
			tSrc.Name(),
			tDst.NumField(),
			tSrc.NumField(),
		)
	}

	srcBytes := C.GoBytes(src, C.int(tSrc.Size()))
	var dstBytes []byte
	oldParts := writeSlice(unsafe.Pointer(&dstBytes), dst, tDst.Size(), tDst.Size())
	defer writeSlice(unsafe.Pointer(&dstBytes), oldParts.ptr, oldParts.len, oldParts.cap)

	for i := 0; i < tDst.NumField(); i++ {
		dstField := tDst.Field(i)
		srcField := tSrc.Field(i)
		copy(
			dstBytes[dstField.Offset:dstField.Offset+dstField.Type.Size()],
			srcBytes[srcField.Offset:srcField.Offset+srcField.Type.Size()],
		)
	}

	return nil
}

func writeSlice(s unsafe.Pointer, p unsafe.Pointer, sz uintptr, cap uintptr) sliceParts {
	raw := sliceParts{
		ptr: p,
		len: sz,
		cap: cap,
	}

	oldParts := sliceParts{
		ptr: (*sliceParts)(s).ptr,
		len: (*sliceParts)(s).len,
		cap: (*sliceParts)(s).cap,
	}
	copy(
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(s))[:],
		(*(*[unsafe.Sizeof(sliceParts{})]byte)(unsafe.Pointer(&raw)))[:],
	)

	return oldParts
}
