package main

import (
	"fmt"
	"io"
	"os"
)

func BadCopyFile1(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}

	written, err = io.Copy(dst, src)
	dst.Close()
	src.Close()
	return
}

func CopyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}
	defer src.Close()

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
}

func a() {
	i := 0
	defer fmt.Println(i)
	i++
	return
}

func b() {
	for i := 0; i < 4; i++ {
		defer fmt.Print(i)
	}
}

func inc(i int) int {
	return i+1
}

func c() (i int) {
	defer func(j int) { j++; fmt.Println("j:", j) }(inc(i))
	return 1
}

func main() {
	a()
	b()
	i := c()
	fmt.Println("main i:", i)
}
