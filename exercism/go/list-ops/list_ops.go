package listops

type unaryFunc func(int) int
type binFunc func(int, int) int
type predFunc func(int) bool

type IntList []int

func (xs IntList) Append(ys IntList) IntList {
	for _, y := range ys {
		xs = append(xs, y)
	}

	return xs
}

func (xs IntList) Foldl(f func (int, int) int, n int) int {
	for _, x := range xs {
		n = f(n, x)
	}
	return n
}

func (xs IntList) Foldr(f func (int, int) int, n int) int {
	for i := len(xs)-1; i >= 0; i-- {
		n = f(xs[i], n)
	}
	return n
}

func (xs IntList) Filter(p func (int) bool) IntList {
	ys := IntList{}
	for _, x := range xs {
		if p(x) {
			ys = append(ys, x)
		}
	}
	return ys
}

func (xs IntList) Length() int {
	length := 0
	for range xs {
		length++
	}
	return length
}

func (xs IntList) Map(f func(int) int) IntList {
	ys := make(IntList, len(xs))
	for i, x := range xs {
		ys[i] = f(x)
	}
	return ys
}

func (xs IntList) Reverse() IntList {
	ysLen := len(xs)
	ys := make(IntList, ysLen)
	for i, x := range xs {
		ys[ysLen-i-1] = x
	}
	return ys
}

func (xs IntList) Concat(yss []IntList) IntList {
	for _, ys := range yss {
		xs = append(xs, ys...)
	}
	return xs
}
