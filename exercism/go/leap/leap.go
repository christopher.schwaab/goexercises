/*
Package leap provides a utility functions relating to leap years.
*/
package leap

// Return true if year is a leap year and false otherwise.
func IsLeapYear(year int) bool {
	// See https://en.wikipedia.org/wiki/Leap_year#Gregorian_calendar
	// for a description of how leap years are defined in the gregorian
	// calendar system.
	if year % 4 != 0 {
		return false
	} else if year % 100 != 0 {
		return true
	} else if year % 400 != 0 {
		return false
	} else {
		return true
	}
}
