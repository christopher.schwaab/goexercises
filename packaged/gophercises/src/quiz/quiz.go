package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
)

type QuizArgs struct {
	CsvFile string
}

type Challenge struct {
	Question string
	Answer string
}

type QuizState struct {
	QuestionCount uint64
	CorrectAnswers uint64
	IncorrectAnswers uint64
}

func parseCsv(file string) (records [][]string, err error) {
	f, ferr := os.Open(file)
	if ferr != nil {
		return [][]string{}, ferr
	}
	defer f.Close()

	return csv.NewReader(f).ReadAll()
}

func (args *QuizArgs) Parse() {
	flag.StringVar(&args.CsvFile, "csv-file", "problems.csv", "The csv formatted file to use as the quiz.")
	flag.Parse()
}

func ParseChallenge(challenge []string) Challenge {
	return Challenge{
		Question: challenge[0],
		Answer: challenge[1],
	}
}

func RenderQuestion(state QuizState, questionNumber uint64, question string) string {
	return fmt.Sprintf(
		"Question %d of %d: %s",
		questionNumber,
		state.QuestionCount,
		question)
}

func readReponse(in *bufio.Reader) (string, error) {
	response, err := in.ReadString('\n')
	if err != nil {
		return response, err
	}
	return response[:len(response)-1], err
}

func (state *QuizState) RunChallenge(in *bufio.Reader, questionNumber uint64, challenge Challenge) {
	fmt.Println(RenderQuestion(*state, questionNumber, challenge.Question))
	fmt.Print("  Answer: ")
	answer, _ := readReponse(in)

	if challenge.Answer == answer {
		fmt.Println("Correct!")
		state.CorrectAnswers++
	} else {
		fmt.Println(fmt.Sprint("`", answer, "', is wrong, the answer is ", challenge.Answer, "."))
		state.IncorrectAnswers++
	}
}

func runTimedQuiz(timeout int, state QuizState, records [][]string) QuizState {
	stdin := bufio.NewReader(os.Stdin)
	for i, challenge := range records {
		state.RunChallenge(stdin, uint64(i)+1, ParseChallenge(challenge))
	}

	return state
}

func main() {
	var args QuizArgs
	args.Parse()

	asyncStdin, _ := NewAsyncStdinReader()
	asyncStdin.AsyncReadString('\n')

	//records, err := parseCsv(args.CsvFile)
	//if err != nil {
	//	fmt.Println("error reading file the file `", args.CsvFile, "':", err)
	//}

	//result := make(chan QuizState)
	//timeout := make(chan QuizState)
	//result := runTimedQuiz(timeout, QuizState{
	//	QuestionCount: uint64(len(records)),
	//	CorrectAnswers: 0,
	//	IncorrectAnswers: 0,
	//})

	//select {
	//case result <-:
	//case timeout <-:
	//}
}
