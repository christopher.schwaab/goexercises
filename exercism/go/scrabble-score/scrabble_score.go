// Package scrabble implements a function for calculating scrabble scores.
package scrabble

import (
	"fmt"
	"unicode"
)

// Score calculates the scrabble score for the given word.
func Score(word string) (score int, err error) {
	score = 0
	for _, c := range word {
		switch unicode.ToUpper(c) {
		case 'A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T':
			score++
		case 'D', 'G':
			score += 2
		case 'B', 'C', 'M', 'P':
			score += 3
		case 'F', 'H', 'V', 'W', 'Y':
			score += 4
		case 'K':
			score += 5
		case 'J', 'X':
			score += 8
		case 'Q', 'Z':
			score += 10
		default:
			return 0, fmt.Errorf("unknown scrabble character '%c'", c)
		}
	}

	return score, nil
}
