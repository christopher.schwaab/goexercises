PWD:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))

all: packaged standalone

update: go.exercises go.gophercises
	for e in $$(cat go.exercises); do \
		if [ ! -e "$(PWD)/exercism/go/$$e" ] ; then \
			exercism download --exercise=$$e --track=go; \
		fi ; \
	done

	for e in $$(cat go.gophercises); do \
		if [ ! -e "$(PWD)/packaged/gophercises/src/$$e" ] ; then \
			git clone https://github.com/gophercises/$$e $(PWD)/packaged/gophercises/src/$$e ; \
		fi ; \
	done

exercism:
	for e in $$(cat go.exercises); do \
		(cd "$(PWD)/exercism/go/$$e" && go test -v --bench . --benchmem) ; \
	done

standalone packaged:
	$(MAKE) -C "$@"

.PHONY: all exercism update standalone packaged
