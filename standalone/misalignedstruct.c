#include "misalignedstruct.h"

struct misaligned_struct mkmisaligned(void) {
    return (struct misaligned_struct) {
        .off0 = 0xdead,
        .off2 = 0xab,
        .off3 = 0xcafeface,
        .off7 = 0xaa,
        .off8 = 0xbeefbeef
    };
}
