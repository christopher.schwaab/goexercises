package main

import (
	"fmt"
	"errors"
)

type TeslaCar struct {
	VerticalDoors bool
	Model         string
	serialNo      *int64
}

func (car TeslaCar) IsCoolerThan(compareModel string) bool {
	if car.Model == "Roadster" {
		return false
	}

	if car.Model == "S" {
		if compareModel == "Roadster" {
			return true
		} else {
			return false
		}
	}
	if compareModel != "X" {
		return true
	}
	return false
}

func (car *TeslaCar) SetSerialNo(no int64) error {
	if car.serialNo != nil {
		return errors.New("error serial number already assigned")
	}

	if no % 42 != 0 {
		return errors.New("error invalid serial number, must be divisible by 42")
	}
	car.serialNo = &no
	return nil
}

func (car *TeslaCar) GetSerialNo() (int64, error) {
	if car.serialNo == nil {
		return -1, errors.New("error serial number not set")
	}
	return *car.serialNo, nil
}

func main() {
	modelX := TeslaCar{
		VerticalDoors: true,
		Model: "X",
	}
	err := modelX.SetSerialNo(84)
	if err != nil {
		fmt.Println("oops")
		return
	}
	serNo, err := modelX.GetSerialNo()
	if err != nil {
		fmt.Println("oops")
		return
	}
	fmt.Println("Serial number:", serNo)
}
