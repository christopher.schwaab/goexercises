// Package darts implements a routine for scoring dart throws.
package darts

var outerRadius int = 10
var middleRadius int = 5
var innerRadius int = 1

func pointInCircle(r int, x, y float64) bool {
	return x*x+y*y <= float64(r*r)
}

// Score computes the dart score for a dart landing at (x, y) on a board with
// three rings having radii 1, 5, and 10.
func Score(x, y float64) int {
	if pointInCircle(innerRadius, x, y) {
		return 10
	} else if pointInCircle(middleRadius, x, y) {
		return 5
	} else if pointInCircle(outerRadius, x, y) {
		return 1
	}

	return 0
}
