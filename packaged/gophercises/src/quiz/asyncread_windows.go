package main

// See https://golang.org/cmd/cgo/

// #include <Windows.h>
import "C"

import (
	"fmt"
	"runtime"
	"syscall"
	"unsafe"
)

//go:cgo_import_dynamic main.sysWaitForMultipleObjects WaitForMultipleObjects%4 "kernel32.dll"
//go:cgo_import_dynamic main.sysReadConsoleInput ReadConsoleInput%4 "kernel32.dll"
//go:cgo_import_dynamic main.sysGetNumberOfConsoleInputEvent GetNumberOfConsoleInputEvent%2 "kernel32.dll"

var sysWaitForMultipleObjects unsafe.Pointer
var sysReadConsoleInput unsafe.Pointer
var sysGetNumberOfConsoleInputEvents unsafe.Pointer

type AsyncStdinReader interface {
	AsyncReadString(c byte) (result <-chan string, cancel func())
}

type asyncStdinReader struct {
	handle syscall.Handle
}

// https://docs.microsoft.com/en-us/windows/console/input-record-str
// typedef struct _INPUT_RECORD {
//   WORD  EventType;
//   union {
//     KEY_EVENT_RECORD          KeyEvent;
//     MOUSE_EVENT_RECORD        MouseEvent;
//     WINDOW_BUFFER_SIZE_RECORD WindowBufferSizeEvent;
//     MENU_EVENT_RECORD         MenuEvent;
//     FOCUS_EVENT_RECORD        FocusEvent;
//   } Event;
// } INPUT_RECORD;
//
// typedef struct _KEY_EVENT_RECORD {
//   BOOL  bKeyDown;
//   WORD  wRepeatCount;
//   WORD  wVirtualKeyCode;
//   WORD  wVirtualScanCode;
//   union {
//     WCHAR UnicodeChar;
//     CHAR  AsciiChar;
//   } uChar;
//   DWORD dwControlKeyState;
// } KEY_EVENT_RECORD;
type unicodeKeyInputRecord struct {
	keyDown bool
	repeatCount uint16
	virtualKeyCode uint16
	virtualScanCode uint16
	utf16LeChar uint16
	controlKeyState uint32
}

func NewAsyncStdinReader() (AsyncStdinReader, error) {
	stdinH, err := syscall.GetStdHandle(C.STD_INPUT_HANDLE)
	return &asyncStdinReader{handle: stdinH}, err
}

// FIXME It looks like there's probably a program to generate this?
// https://golang.org/src/internal/syscall/windows/mksyscall.go
func waitForMultipleObjects(n uint32, hs []syscall.Handle, waitForAll bool, timeoutMs uint32) {
	var lpHandles *syscall.Handle
	lpHandles = &hs[0]
	switch runtime.GOARCH {
	case "amd64":
		// https://docs.microsoft.com/en-gb/windows/win32/api/synchapi/nf-synchapi-waitformultipleobjects
		// DWORD WaitForMultipleObjects(
	  	//   DWORD        nCount,
	  	//   const HANDLE *lpHandles,
	  	//   BOOL         bWaitAll,
	  	//   DWORD        dwMilliseconds
	  	//);
		bWaitAll := int32(0)
		if waitForAll {
			bWaitAll = 1
		}
		a, b, c := syscall.Syscall6(
			uintptr(sysWaitForMultipleObjects),
			4,
			uintptr(n),
			// Apparently wrapping this in an unsafe.Pointer call under a Syscall call
			// will pin the slice for the lifetime of the call so this should be safe:
			//   https://golang.org/pkg/unsafe/
			uintptr(unsafe.Pointer(lpHandles)),
			uintptr(bWaitAll),
			uintptr(timeoutMs),
			0,
			0)

	default:
		panic("WaitForMultipleObjects is only implemented for amd64")
	}
}

func readConsoleInput(consoleHandle syscall.Handle, maxEvents uint32) ([]rawInputRecord, error) {
	// https://docs.microsoft.com/en-us/windows/console/readconsoleinput
	//BOOL WINAPI ReadConsoleInput(
	//	_In_  HANDLE        hConsoleInput,
	//	_Out_ PINPUT_RECORD lpBuffer,
	//	_In_  DWORD         nLength,
	//	_Out_ LPDWORD       lpNumberOfEventsRead
	//);
	var numberOfEventsRead uint32 = 0
	inputs := make([]rawInputRecord, maxEvents)
	_, _, err := syscall.Syscall6(
		uintptr(sysReadConsoleInput),
		3,
		uintptr(unsafe.Pointer(&inputs[0])),
		uintptr(len(inputs)),
		uintptr(unsafe.Pointer(&numberOfEventsRead)),
		0,
		0,
		0)
	if err != 0 {
		return []rawInputRecord{}, err
	}
	return inputs, err
}

func getNumberOfConsoleInputEvents(h syscall.Handle) (uint32, error) {
	// https://docs.microsoft.com/en-us/windows/console/getnumberofconsoleinputevents
	//BOOL WINAPI GetNumberOfConsoleInputEvents(
	//  _In_  HANDLE  hConsoleInput,
	//  _Out_ LPDWORD lpcNumberOfEvents
	//);
	var numberOfEvents uint32 = 0
	_, _, err := syscall.Syscall(
		uintptr(sysGetNumberOfConsoleInputEvents),
		2,
		uintptr(unsafe.Pointer(h)),
		uintptr(unsafe.Pointer(&numberOfEvents)),
		0)

	return numberOfEvents, err
}

func (r *asyncStdinReader) AsyncReadString(c byte) (result <-chan string, cancel func()) {
	result = make(chan string)
	//cancellationChan := syscall.CreateIoCompletionPort(0, 0, 0, 0)

	//go func() {
	//	sysos.stdcall4(
	//		waitForMultipleObjects,
	//		1,
	//		[]syscall.Handle{r.handle},
	//		0,
	//		0)
	//}()

	waitForMultipleObjects(1, []syscall.Handle{r.handle}, false, 1000)

	return result, func () {
		fmt.Println("hi")
	}
}
