package scrabble

// Source: exercism/problem-specifications
// Commit: 0d882ed scrabble-score: Apply new "input" policy
// Problem Specifications Version: 1.1.0

type scrabbleTest struct {
	input       string
	expected    int
	errExpected bool
}

var scrabbleScoreTests = []scrabbleTest{
	{"a", 1, false},                           // lowercase letter
	{"A", 1, false},                           // uppercase letter
	{"f", 4, false},                           // valuable letter
	{"at", 2, false},                          // short word
	{"zoo", 12, false},                        // short, valuable word
	{"street", 6, false},                      // medium word
	{"quirky", 22, false},                     // medium, valuable word
	{"OxyphenButazone", 41, false},            // long, mixed-case word
	{"pinata", 8, false},                      // english-like word
	{"", 0, false},                            // empty input
	{"abcdefghijklmnopqrstuvwxyz", 87, false}, // entire alphabet available
	{"+", 0, true},                            // non-scrabble, single-byte rune
	{"ァ", 0, true},                           // non-scrabble, multi-byte rune
}
