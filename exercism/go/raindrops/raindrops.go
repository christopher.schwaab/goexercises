/*
Package raindrops exposes a utility function for converting integers into
a ranindrop sound string.
*/
package raindrops

import (
	"strconv"
	"strings"
)

// Convert converts an integer z into an integral string or a series of raindrop
// sounds based on its factors.
func Convert(z int) string {
	var messageBuilder strings.Builder
	if z%3 == 0 {
		messageBuilder.WriteString("Pling")
	}
	if z%5 == 0 {
		messageBuilder.WriteString("Plang")
	}
	if z%7 == 0 {
		messageBuilder.WriteString("Plong")
	}

	message := messageBuilder.String()
	if message == "" {
		message = strconv.Itoa(z)
	}

	return message
}
