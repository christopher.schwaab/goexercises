// Package acronym implements a routine for constructing acronyms from sentences.
package acronym

import (
	"unicode"
)

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	acronym := make([]rune, len(s)/2 + 1)
	findWord := true
	i := 0
	for _, c := range s {
		if findWord {
			if unicode.IsLetter(c) {
				acronym[i] = unicode.ToUpper(c)
				i++
				findWord = false
			}
		} else if unicode.IsSpace(c) || c == '-' {
			findWord = true
		}
	}

	return string(acronym[:i])
}
