package opaquestr

import "fmt"

type Str string

type Str2 = string

type pStr string

func MkStr(s string) Str {
	return Str(s)
}

func PrintStr(s Str) {
	fmt.Println("private str is:", s)
}

func MkStr2(s string) Str2 {
	return Str2(s)
}

func PrintStr2(s Str2) {
	fmt.Println("private str2 is:", s)
}

func MkPStr(s string) pStr {
	return pStr(s)
}

func PrintPStr(s pStr) {
	fmt.Println("private pStr is:", s)
}
