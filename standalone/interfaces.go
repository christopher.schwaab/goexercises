package main

import (
	"fmt"
	"errors"
)

type YMover interface {
	MoveY(int64) error
}

type XMover interface {
	MoveX(amoutn int64) (err error)
}

type ZMover interface {
	MoveZ(amount int64) (err error)
}

type FlatMover interface {
	YMover
	XMover
}

type Mover interface {
	YMover
	XMover
	ZMover
}

type Car struct {
	x, y int64
}

func (car *Car) MoveX(amt int64) (err error) {
	car.x += amt
	return
}

func (car *Car) MoveY(amt int64) (err error) {
	car.y += amt
	return
}

type Airplane struct {
	x, y, z int64
}

func (plane *Airplane) MoveX(amt int64) (err error) {
	plane.x += amt
	return
}

func (plane *Airplane) MoveY(amt int64) (err error) {
	plane.y += amt
	return
}

func (plane *Airplane) MoveZ(amt int64) (err error) {
	if plane.z + amt > 50000 {
		return errors.New("error z too big")
	}
	if plane.z + amt < 0 {
		return errors.New("error z too little")
	}

	plane.z += amt
	return
}

func PerformFlatMovement(flatMover FlatMover) error {
	fmt.Println("[FLAT/FULL] Move y by +42")
	if err := flatMover.MoveY(42); err != nil {
		return err
	}
	fmt.Println("[FLAT/FULL] Move x by -42")
	if err := flatMover.MoveX(-42); err != nil {
		return err
	}
	return nil
}

func PerformFullMovement(fullMover Mover) error {
	if err := PerformFlatMovement(fullMover); err != nil {
		return err
	}
	fmt.Println("[FULL] Move z by +21")
	if err := fullMover.MoveZ(21); err != nil {
		return err
	}
	return nil
}

func main() {
	myPlane := Airplane{}
	myCar := Car{}

	fmt.Println("myCar before movement: ", myCar)
	PerformFlatMovement(&myCar)
	fmt.Println("myCar after movement: ", myCar)

	fmt.Println("myPlane before movement: ", myPlane)
	PerformFullMovement(&myPlane)
	fmt.Println("myPlaneafter movement: ", myPlane)
}
